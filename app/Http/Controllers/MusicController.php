<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelMusic;
use App\Http\Requests\MusicRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musics=ModelMusic::all();
        return view('music.index',compact('musics'));



    }

    public function indexCustomer(){
        $musics=ModelMusic::all();
        return view('music.indexcustomer',compact('musics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('music.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MusicRequest $request)
    {       

        // $image=$request->file('music_image');
        // $filename=$image->getClientOriginalName();
        // storage::put('upload/music_image/'.$filename,file_get_contents($image->getRealPath()));
        // ModelMusic::create($request->all());
        // return redirect()->route('music.index')->with('message','item has been added');

        If(Input::hasFile('music_image')){
            

            $file = Input::file('music_image');

            $destinationPath = public_path(). '/upload/music_image';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);


            }

            ModelMusic::create([
                'music_name'=>$request->music_name,
                'music_image' => $filename,
                'music_price'=>$request->music_price
            ]);
            
        //     ModelMusic::create($request->all());
        return redirect()->route('music.index')->with('message','item has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ModelMusic $music)
    {
         return view('music.show',compact('music'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelMusic $music)
    {
        return view('music.edit',compact('music'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MusicRequest $request, ModelMusic $music)
    {   

         $musics=compact('music');
        foreach ($musics as $music) {

            $path=$_SERVER['DOCUMENT_ROOT'].'/musiclibrary/public/upload/music_image/'.$music->music_image;
             if(file_exists($path)){
                unlink($path);
            }
        }
        If(Input::hasFile('music_image')){
            

            $file = Input::file('music_image');

            $destinationPath = public_path(). '/upload/music_image';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);


            }
            $arr=array(
                'music_name' => $request->music_name,
                 'music_image' => $filename,
                 'music_price'=>$request->music_price
                 );
            
            // ModelMusic::update([
                
            //     'music_name'=>$request->music_name,
            //     'music_image' => $filename,
            //     'music_price'=>$request->music_price
            // ]);
        
        $music->update($arr);
        return redirect()->route('music.index')->with('message','item has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelMusic $music)
    {
        // $musics=ModelMusic::all();
        $musics=compact('music');
        foreach ($musics as $music) {

            $path=$_SERVER['DOCUMENT_ROOT'].'/musiclibrary/public/upload/music_image/'.$music->music_image;
             if(file_exists($path)){
                unlink($path);
            }
        
            $music->delete();
            return redirect()->route('music.index')->with('message','item has been deleted');
    }
    }

   
}