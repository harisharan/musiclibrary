<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelCustomer;
use App\ModelAdmin;
use App\Http\Requests\CustomerRequest;
use Validator;
use Auth;
use Session;

class CustomerController extends Controller
{
        public function login(){
           return view('login');
        }

        public function logout(){
            Session::flush();
            return redirect('/')->with('error','log out');
        }
        public function message(){
             echo 'work in progress...';
        }

        public function checkLogin(request $request){
            $email = $request->input('email');
            $pword=$request->input('password');
            $usertype=$request->input('usertype');
            if($usertype=="admin"){
             $modeladmin=new ModelAdmin();
             $admindata=$modeladmin->getAdminData($email);
        
      
            foreach ($admindata as $admin) {
            $db_email=$admin->admin_email;
             $db_pword=$admin->admin_password;
             
            
             if($email=$db_email && $pword=$db_pword){
                
                // session_start();
                // $_SESSION["sess_id"]=$customer->id;
                session(['sess_id' => $admin->id]);
                return redirect('customer')->with('message','login successful');

        //     $customers=ModelCustomer::all();
        // return view('customer.index',compact('customers'));
                }
             }
         }else{
            $modelcustomer=new ModelCustomer();
             $customerdata=$modelcustomer->getCustomerData($email);
             
      
            foreach($customerdata as $customer){
            $db_email=$customer->customer_email;
             $db_pword=$customer->customer_password;
             
            
             if($email=$db_email && $pword=$db_pword){
                
                // session_start();
                // $_SESSION["sess_id"]=$customer->id;
                session(['sess_c_id' => $customer->id]);
                return redirect('indexcustomer')->with('message','login successful');

        //     $customers=ModelCustomer::all();
        // return view('customer.index',compact('customers'));
                }
             }
         }
        return redirect('/')->with('error','login fail');
        
         }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=ModelCustomer::all();
        return view('customer.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        ModelCustomer::create($request->all());
        return redirect()->route('customer.index')->with('message','customer has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ModelCustomer $customer)
    {
          return view('customer.show',compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelCustomer $customer)
    {
        return view('customer.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, ModelCustomer $customer)
    {
    
$customer->update($request->all());
        return redirect()->route('customer.index')->with('message','customer has been updated');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelCustomer $customer)
    {
        $customer->delete();
        return redirect()->route('customer.index')->with('message','customer has been deleted');
    }
}
