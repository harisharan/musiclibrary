<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\MusicCustomerModel;
class CustomerMusicController extends Controller
{
    public function buy(Request $request){
    		$musicid=$request->get('id');
    		$customer_id=Session::get('sess_c_id');

    		$arr=array(
    			'music_id' => $musicid, 
    			'customer_id'=>$customer_id,
    			);
    		MusicCustomerModel::create($arr);
    		 return redirect('indexcustomer')->with('message','buying successful...please ensure payment?');

    }

    public function index(){
    	$musiccustomermodel=new MusicCustomerModel();
    	$buylist=$musiccustomermodel->buyingName();

    	// $buylist=MusicCustomerModel::all();
        return view('music.buyinglist',compact('buylist'));
    }
}
