<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelMusic extends Model
{
    protected $fillable=['id','music_name','music_image','music_price'];
}
