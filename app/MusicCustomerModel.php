<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MusicCustomerModel extends Model
{
    protected $fillable=['music_id','customer_id'];

    public function buyingName(){
    	return DB::select( DB::raw(
    		"SELECT mc.customer_name,mm.music_name FROM
    		 music_customer_models mcm inner join model_customers mc on mcm.customer_id=mc.id
    									inner join model_musics mm on mcm.music_id=mm.id"
    		));
    }
}
