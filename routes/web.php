<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/entry', function () {
//     return view('CustomerEntry');
// });

// Route::resource('create', 'CustomerController@create@store');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/', 'CustomerController@login');


Route::resource('music','MusicController');
Route::get('indexcustomer','MusicController@indexCustomer');
Route::resource('customer','CustomerController');
Route::post('checklogin','CustomerController@checkLogin');
Route::get('logout','CustomerController@logout');
Route::get('buy','CustomerMusicController@buy');
Route::resource('buylist','CustomerMusicController');

