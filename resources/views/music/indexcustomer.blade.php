
<?php
if(Session::get('sess_c_id')){
    
    ?>@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">MUSICS</div>
                @if(Session::has('message'))
                <div class="alert alert-success">
                	
                	{{ Session::get('message') }}
                </div>
                @endif
                <div class="panel-body">
                <table class="table table-responsive">
                	<tr>
                			<th>music id</th>
                			<th>music name</th>
                			<th>music image</th>
                			<th>music price</th>
                			<th colspan="2">action</th>
                	</tr>
                	@foreach($musics as $music)
                	<tr>
                			<td>{{ $music->id }}</td>
                			<td>
                                {{ link_to_route('music.show',$music->music_name,[$music->id]) }}
                            </td>
                			<td><img src="public/upload/music_image/{{ $music->music_image }}" width="200"/></td>
                			<td>{{ $music->music_price }}</td>
                			  <td><a class="navbar-brand" href="{{ url('buy') }}?id={{$music->id}}">
                        buy
                    </a>
                    </td>
                			
                            
                	</tr>
                	 @endforeach
                	</table>
                   			
                   			
                  
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
 <?php 
}else{
    echo 'login as customer...not for admin';
}
?>