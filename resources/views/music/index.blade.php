
<?php
if(Session::get('sess_id')){

    ?>
    @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">MUSICS</div>
                @if(Session::has('message'))
                <div class="alert alert-success">
                	
                	{{ Session::get('message') }}
                </div>
                @endif
                <div class="panel-body">
                <table class="table table-responsive">
                	<tr>
                			<th>music id</th>
                			<th>music name</th>
                			<th>music image</th>
                			<th>music price</th>
                			<th colspan="2">action</th>
                	</tr>
                	@foreach($musics as $music)
                	<tr>
                			<td>{{ $music->id }}</td>
                			<td>
                                {{ link_to_route('music.show',$music->music_name,[$music->id]) }}
                            </td>
                			<td><img src="public/upload/music_image/{{ $music->music_image }}" width="200"/></td>
                			<td>{{ $music->music_price }}</td>
                			<td>{{ link_to_route('music.edit','edit',[$music->id],['class'=>'btn btn-primary']) }}</td>
                			{!! Form::model($music,array('route'=>['music.destroy',$music->id],'method'=>'DELETE')) !!}
                			<td>	{!! Form::submit('delete',['class'=>'btn btn-warning']) !!}</td>
                			{!! Form::close() !!}
                            
                	</tr>
                	 @endforeach
                	</table>
                   			
                   			
                  
                </div>
                <div class="panel-footer">
                	
                	{{ link_to_route('music.create','add new music',null,['class'=>'btn btn-primary']) }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 <?php 
}else{
    echo 'login first and authority for admin';
}
?>