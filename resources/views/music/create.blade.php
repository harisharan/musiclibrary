<?php
if(Session::get('sess_id')){

    ?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Music entry form</div>

                <div class="panel-body">
               
                	{!! Form::open(['route'=>'music.store','class'=>'form-horizontal','enctype'=>'mutlipart/form-data','files'=>true]) !!}
                	 <div class="form-group">
                	{!! Form::label('music_name','Music Name',['class'=>'control-label']) !!}
					{!! Form::text('music_name',null,['class'=>'form-control']) !!}
					</div>
					 <div class="form-group">
                	{!! Form::label('music_image','Music image',['class'=>'control-label']) !!}
					{!! Form::file('music_image',null,['class'=>'form-control']) !!}

					</div>
					 <div class="form-group">
                	{!! Form::label('music_price','Music price',['class'=>'control-label']) !!}
					{!! Form::number('music_price',null,['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit('save',['class'=>'btn btn-primary']) !!}
					</div>
                	{!! Form::close() !!}
              
            </div>
        </div>
    </div>
</div>
@endsection
<?php 
}else{
    echo 'login first and authority for admin';
}
?>