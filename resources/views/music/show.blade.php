<?php
if(Session::get('sess_id')){

    ?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h5><b>music name:</b></h5><b>{{ $music->music_name }}</b></div>

                <div class="panel-body">
                <div class="form-group">

               <h5><b>music image:</b></h5><img src="{{ asset('public/upload/music_image/'.$music->music_image) }}" width="200"/>
               </div>
            
               <div class="form-group">
               <h5><b>music price:</b></h5><b>{{ $music->music_price }}</b>
             
               </div>
              
            </div>
        </div>
    </div>
</div>
@endsection
<?php 
}else{
    echo 'login first and authority for admin';
}
?>