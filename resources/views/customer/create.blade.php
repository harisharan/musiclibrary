<?php
if(Session::get('sess_id')){
    ?>
   
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">customer entry form</div>

                <div class="panel-body">
               
                    {!! Form::open(['route'=>'customer.store','class'=>'form-horizontal']) !!}
                     <div class="form-group">
                    {!! Form::label('customer_name','customer Name',['class'=>'control-label']) !!}
                    {!! Form::text('customer_name',null,['class'=>'form-control']) !!}
                    </div>
                     <div class="form-group">
                    {!! Form::label('customer_address','customer address',['class'=>'control-label']) !!}
                    {!! Form::text('customer_address',null,['class'=>'form-control']) !!}

                    </div>
                     <div class="form-group">
                    {!! Form::label('customer_email','customer email',['class'=>'control-label']) !!}
                    {!! Form::text('customer_email',null,['class'=>'form-control']) !!}
                    </div>
                     <div class="form-group">
                    {!! Form::label('customer_password','customer password',['class'=>'control-label']) !!}
                    {!! Form::text('customer_password',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('save',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
              
            </div>
        </div>
    </div>
</div>
@endsection
 <?php 
}else{
    echo 'login first and authority for admin';
}
?>