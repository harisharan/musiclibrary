<?php
if(Session::get('sess_id')){
    ?>
   
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update customer entry form</div>

                <div class="panel-body">
               
                	{!! Form::model($customer,array('route'=>['customer.update',$customer->id],'method'=>'PUT','enctype'=>'mutlipart/form-data','files'=>true)) !!}
                	 <div class="form-group">
                	{!! Form::label('customer_name','customer Name',['class'=>'control-label']) !!}
					{!! Form::text('customer_name',null,['class'=>'form-control']) !!}
					</div>
					 <div class="form-group">
                	{!! Form::label('customer_address','customer image',['class'=>'control-label']) !!}
					{!! Form::text('customer_address',null,['class'=>'form-control']) !!}
					</div>
					 <div class="form-group">
                	{!! Form::label('customer_email','customer price',['class'=>'control-label']) !!}
					{!! Form::text('customer_email',null,['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
                	{!! Form::label('customer_password','customer price',['class'=>'control-label']) !!}
					{!! Form::text('customer_password',null,['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit('update',['class'=>'btn btn-primary']) !!}
					</div>
                	{!! Form::close() !!}
              
            </div>
        </div>
    </div>
</div>
@endsection
 <?php 
}else{
    echo 'login first and authority for admin';
}
?>