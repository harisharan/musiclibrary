<?php
if(Session::get('sess_id')){
    ?>
   

 

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">customers</div>
                @if(Session::has('message'))
                <div class="alert alert-success">
                	
                	{{ Session::get('message') }}
                </div>
                @endif
                <div class="panel-body">
                <table class="table table-responsive">
                	<tr>
                			<th>customer id</th>
                			<th>customer name</th>
                			<th>customer address</th>
                			<th>customer email</th>
                			<th colspan="2">action</th>
                	</tr>
                	@foreach($customers as $customer)
                	<tr>
                			<td>{{ $customer->id }}</td>
                			<td>
                                {{ link_to_route('customer.show',$customer->customer_name,[$customer->id]) }}
                            </td>
                		
                			<td>{{ $customer->customer_address }}</td>
                            <td>{{ $customer->customer_email }}</td>

                			<td>{{ link_to_route('customer.edit','edit',[$customer->id],['class'=>'btn btn-primary']) }}</td>
                			{!! Form::model($customer,array('route'=>['customer.destroy',$customer->id],'method'=>'DELETE')) !!}
                			<td>	{!! Form::submit('delete',['class'=>'btn btn-warning']) !!}</td>
                			{!! Form::close() !!}
                            
                	</tr>
                	 @endforeach
                	</table>
                   			
                   			
                  
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
 <?php 
}else{
    echo 'login first and authority for admin';
}
?>