@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-lg-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Login
            @if(Session::has('error'))
                <div class="alert alert-success">
                    
                    {{ Session::get('error') }}
                </div>
                @endif
                </div>

                <div class="panel-body">




                 {!! Form::open(['url'=>'checklogin','class'=>'form-horizontal']) !!}
                    
                     <div class="form-group">
                    {!! Form::label('email','email',['class'=>'control-label']) !!}
                    {!! Form::text('email',null,['class'=>'form-control']) !!}
                    </div>
                     <div class="form-group">
                    {!! Form::label('password','password',['class'=>'control-label']) !!}
                    {!! Form::text('password',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="usertype">
                            <option>admin</option>
                            <option>customer</option>
                        </select>
                    </div>
                    <div class="form-group pull-right" style="margin-top:10px;">
                        {!! Form::submit('save',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                   <!--  <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form> -->
                     {{ link_to_route('customer.create','new customer',null) }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
